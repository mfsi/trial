using System;
using System.Collections.Generic;

class Customer : IEquatable<Customer>,IComparable<Customer>
{
	public static int opt=0;
    public int CustomerId { get; set; }
    public string First { get; set; }
	 public string Last { get; set; }
    public int Age { get; set; }
	 public string Address { get; set; }
   
   
	
		public override string ToString()
        {
				return "Cid: " + CustomerId + "   Name: " + First + " last: "+ Last +" Age:" +Age + " Add: " + Address;
        }
  
		public override bool Equals(object obj)
        {
				if (obj == null) return false;
				Customer objAsCustomer = obj as Customer;
				if (objAsCustomer == null) return false;
				else return Equals(objAsCustomer);
        }
		
        public override int GetHashCode()
        {
            return CustomerId;
        }
        public bool Equals(Customer other)
        {
            if (other == null) return false;
            return (this.CustomerId.Equals(other.CustomerId));
        }
		public int CompareTo(Customer compareCustomer)
				{
					if (compareCustomer == null)
									return 1;
					 switch(opt)
					 {
					    case 1:
									return this.CustomerId.CompareTo(compareCustomer.CustomerId);
									
						case 2:
									return this.First.CompareTo(compareCustomer.First);
									
						case 3:
									return this.Age.CompareTo(compareCustomer.Age);
									
						case 4:
									return this.Last.CompareTo(compareCustomer.Last);
									
						default:
						return this.CustomerId.CompareTo(compareCustomer.CustomerId);
									
					 }
				}
		
	 
  
}

	
	
	



class ListProgram
{
    static void Main()
    {
	List<Customer> list = new List<Customer>();
	//adding();
	
	 Console.WriteLine("enter the no of records:");
     int n=Convert.ToInt32(Console.ReadLine());
     string first,last,address;
	 int age,custid;
	for(int i=0;i<n;i++)
					{
						Console.WriteLine("Enter the CustomerId");
						 custid=Convert.ToInt32(Console.ReadLine());
						Console.WriteLine("Enter the FirstName");
						first=Console.ReadLine();
						Console.WriteLine("Enter the LastName");
						 last=Console.ReadLine();
						Console.WriteLine("Enter the Age");
						 age=Convert.ToInt32(Console.ReadLine());
						Console.WriteLine("Enter the Address");
						 address=Console.ReadLine();
					 //var Cus = new Customer(custid,first,last,age,address);	
					   list.Add(new Customer(){ CustomerId = custid, First = first, Last=last, Age=age, Address=address });
					  // list.Add(new Employee() { Cid = cid, First = first, Last=last, Age=age, Add=add });
					}
					list.Sort();
					//Display After Sorting
					foreach (var element in list)
					{
						Console.WriteLine(element);
					}
					//Removing
					
					Console.WriteLine("Enter the Customer Id to be removed");
				   int rem=Convert.ToInt32(Console.ReadLine());
					list.Remove(new Customer(){CustomerId=rem});
					
					foreach (Customer CustomerId in list)
					{
						Console.WriteLine(CustomerId);
					}
	
	
				//Searching
						Console.WriteLine("Enter to search  \n 1  by Id \n 2  by Firstname\n 3 by Age");
						int srch=Convert.ToInt32(Console.ReadLine());
						switch(srch)
						{
							case 1:
										{
											Console.WriteLine("Enter the Customer Id to search");
											int search=Convert.ToInt32(Console.ReadLine());
											 if(list.Contains(new Customer {CustomerId=search}))
												 {
													 
													foreach (Customer cust in list )
													{
														if(cust.CustomerId==search)
														{
														Console.WriteLine(cust);
														}
														
													} 
												 }
												
											break;
										}
							case 2:
										{
											Console.WriteLine("Enter the Customer FirstName to search");
											string search=Console.ReadLine();
											 if(list.Contains(new Customer {First=search}))
												 {
													 
													foreach (Customer cust in list )
													{
														if(cust.First==search)
														{
														Console.WriteLine(cust);
														}
														
													} 
												 }
												 
											break;
										}
							case 3:{
											Console.WriteLine("Enter the Customer Age to search");
											int  search=Convert.ToInt32(Console.ReadLine());
											 if(list.Contains(new Customer {Age=search}))
												 {
													 foreach (Customer cust in list )
													{
													
														if(cust.Age==search)
														{
														Console.WriteLine(cust);
														}
														
													}
												 }
												
												
											break;
										}
							
						}
						
					Console.WriteLine("----------------------");	
					/*		
					foreach (Customer cust2 in list)
					{
						Console.WriteLine(cust2);
					}
					*/
				
	
	
    }
	
}
	




