using Interface;
using System;
namespace Voice
{
public class VoiceMessage:IMessage
{
  private string recordedMessage;	
  private string sentMessage;
  private string receivedMessage;
  
  public void UpdateDatabase()
  {
	 Console.WriteLine("Updating database");
  }
  public void GetConnection()
  {
	  Console.WriteLine("Getting connection");
  }
  public void BroadcastMessage()
  {
	  Console.WriteLine("Broadcasting Message");
  }
  
  public void Record()
  {
	  Console.WriteLine("Enter message to be sent");
	  this.recordedMessage=Console.ReadLine();
	  Console.WriteLine("Recording message");
	  
  }
  
  public void Receive(string message)
  {
	  Console.WriteLine("Receiving message");
	  this.receivedMessage=sentMessage;
	  
  }
  public string Send()
  {
	   Console.WriteLine("Sending message");
	   this.sentMessage=recordedMessage;
	  return this.sentMessage;
  }
  
  
}

}