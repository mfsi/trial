namespace Interface
{
public interface IMessage
{
	void Send(string message);
	
	string Receive();
	
	string Provider { get; set; }
	
	string SenderName { get; set; }
}

public interface IEmail
	: IMessage
{
	string To { get; set; }
	
	string Subject { get; set; }
}
public interface IWhatsapp
	: IMessage, IVoiceMessage
{
	string To { get; set; }
	string BroadCast { get; set; }
	void CreateGroup();
	bool Deletemsg{ get; set; }
	
	
}
public interface ISMS_Sending
	: IMessage
{
	string To { get; set; }
	 void Forward(string message);
	string From { get; set; }
	
}

public interface IVoiceMessage
	: IMessage
{
	string To { get; set; }
	void Forward(string message);
	
	bool Deletemsg{ get; set; }
}
public interface IBlog
	: IMessage
{
	
	 void Time(string message);
	
	
}
public interface IMessenger
	: IMessage
{
	string To { get; set; }
	string From { get; set; }
		bool Deletemsg{ get; set; }
	void Time(string message);
	
	
}
}


