using System;
using Interface;


public class Imlementation
{
	public static void Main()
	{
		Console.WriteLine("1=>Email,2=>Whatsapp,3=>Messenger||4=>Blog||5=>SMS||6=>Voice Message");
		int opt=Convert.ToInt32(Console.ReadLine());
		IMessage ms= null;
		switch(opt)
		{
			case 1:
				 ms = new Email("Comments","Neha","Ashia","Assignment1");
				ms.Send("Pooja");
				ms.Receive();
				break;
			case 2:
				IWhatsapp ms1=new Whatsapp("Invoice","Shivani","Suman","true",true);
				ms1.Send("Neha");
				ms1.Receive();
				ms1.Forward("ABC");
				break;
			case 3:
				 ms=new Messenger("ContactUs","Lisa","Neha","Aftab",false);
				ms.Send("Rahul");
				ms.Receive();
				break;
			case 4:
				 ms=new Blog("Registration","Rahul");
				ms.Send("Manoj");
				ms.Receive();
				break;
			case 5:
				 ms=new SMS_Sending("Ratting","Asim","Aftab","Yasmin");
				ms.Send("Ashia");
				ms.Receive();
				break;
			case 6:
				IVoiceMessage ms5=new VoiceMessage("FacebookPost","Ashia","Pooja",true);
				ms5.Send("Piyush");
				ms5.Receive();
				ms5.Forward("XYZ");
				break;		
			default:
				Console.WriteLine("Invalid Choice");
				break;
		}
		
		Console.WriteLine("Calling the dummy classes that are using  IMessage ");
		var invoice = new Invoice(ms);
		var contactUs = new ContactUs(ms);
		var rating = new Rating(ms);
		var registration = new Registration(ms);
		var facebookpost = new FacebookPost(ms);
		var comments = new Comments(ms);
		
	}
}
public class Email
	: IEmail
{
	public Email(string p,string sendnm,string to,string subject)
	{
		Provider=p;
		SenderName=sendnm;
		To=to;
		Subject=subject;
		
	}
	public void Send(string message)
	{
		Console.WriteLine("Email message sent"+ message);
	}
	
	public string Receive()
	{
		Console.WriteLine("Email message recieved");
		return "hi";
	}
	
	public string Provider { get; set; }
	
	public string SenderName { get; set; }
	public string To { get; set; }
	
	public string Subject { get; set; }
}
public class VoiceMessage
	: IVoiceMessage
{
	public  VoiceMessage(string p,string sendnm,string to,bool delete)
	{
		Provider=p;
		SenderName=sendnm;
		To=to;
		Deletemsg=delete;
		
	}
	public void Send(string message)
	{
		Console.WriteLine("Voice message sent");
	}
	
	public string Receive()
	{
		Console.WriteLine("Voice message received");
		return "hi";
	}
	
	public string Provider { get; set; }
	
	public string SenderName { get; set; }
	public string To { get; set; }
	public void Forward(string message)
	{
		Console.WriteLine("Voice message forwarded");
	}
	
	public bool Deletemsg{ get; set; }
}
public class Whatsapp
	: IWhatsapp
{
	public Whatsapp(string p,string sendnm,string to,string broadcast,bool delete)
	{
		Provider=p;
		SenderName=sendnm;
		To=to;
		BroadCast=broadcast;
		
		Deletemsg=delete;
		
	}
	
	public void Forward(string msg)
	{
		Console.WriteLine("Whatsapp message Forwarded!");
	}
	
	public void Send(string message)
	{
		Console.WriteLine("Whatsapp message sent");
	}
	public string Receive()
	{
		Console.WriteLine("Whatsapp message received");
		return "hi";
	}
	
	
	public string Provider { get; set; }
	
	public string SenderName { get; set; }
	public string To { get; set; }
	public string BroadCast { get; set; }
	public void CreateGroup()
	{
		Console.WriteLine("Whatsapp group created");
	}
	public bool Deletemsg{ get; set; }
}
public class Blog
	: IBlog
{
	public Blog(string p,string sendnm)
	{
		Provider=p;
		SenderName=sendnm;
		
		
	}
	public void Send(string message)
	{
		Console.WriteLine("Blog Send");
	}
	
	public string Receive()
	{
		Console.WriteLine("Blog Received");
		return "hi";
	}
	
	public string Provider { get; set; }
	
	public string SenderName { get; set; }
	public void Time(string message)
			{
				Console.WriteLine("Time will be displayed here");
			}
}
public class Messenger
	: IMessenger
{
	public Messenger(string p,string sendnm,string to,string from,bool delete)
	{
		Provider=p;
		SenderName=sendnm;
		To=to;
		From=from;
		Deletemsg=delete;
		
	}
	public void Send(string message)
	{
		Console.WriteLine("Messenger message send");
	}
	
	public string Receive()
	{
		Console.WriteLine("Messenger message Receive");
		return "hi";
	}
	
	public string Provider { get; set; }
	
	public string SenderName { get; set; }
	public string To { get; set; }
	public string From { get; set; }
	public bool Deletemsg{ get; set; }
	public void Time(string message)
			{
				Console.WriteLine("Time will be displayed here");
			}
}
public class SMS_Sending
	: ISMS_Sending
{
	public SMS_Sending(string p,string sendnm,string to,string from)
	{
		Provider=p;
		SenderName=sendnm;
		To=to;
		From=from;
		
		
	}
	public void Send(string message)
	{
		Console.WriteLine("SMS sent");
	}
	
	public string Receive()
	{
		Console.WriteLine("SMS Recieved");
		return "hi";
	}
	public string Provider { get; set; }
	
	public string SenderName { get; set; }
	public string To { get; set; }
	public void Forward(string message)
			{
				Console.WriteLine("SMS is Forwarded");
			}
	public string From { get; set; }
}


public class Invoice
{
	public Invoice(IMessage msg)
	{
		msg.Send("Invoice");
	}
}

public class ContactUs
{
	public IMessage Msg { get; set; } 
	public ContactUs(IMessage msg)
	{
		msg.Send("ContactUs");
	}
}
public class Registration
{
	public Registration(IMessage msg)
	{
		msg.Send("Registration");
	}
}
public class FacebookPost
{
	public FacebookPost(IMessage msg)
	{
		msg.Send("FacebookPost");
	}
}
public class Comments
{
	public Comments(IMessage msg)
	{
		msg.Send("Comments");
	}
}
public class Rating
{
	public Rating(IMessage msg)
	{
		msg.Send("Rating");
	}
}
