using Interface;
using System;
namespace MyBlog
{
	public class Blog:IMessage
	{
		public void Receive(string message)
		{
			Console.WriteLine("After posting information on blog: ");
			Console.WriteLine(message);
		}
		
		public string Send()
		{
			Console.WriteLine("Enter information that you want to post in your Blog: ");
			string read=Console.ReadLine();
			return read;
		}
	}
}