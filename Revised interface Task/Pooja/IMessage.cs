using System;
namespace IMessageNamespace
{
	public interface IMessage{
		string Send();
		void Receive(string ReceivedMessage);
	}
}