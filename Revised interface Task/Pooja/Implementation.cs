using System;
using IMessageNamespace;
using MyEmail;

class Program
{
	static void Main(string[] args)
	{
		//Email Implementation
		Email msg = new Email();
		string sender = msg.From();
		string reciever= msg.To();
		string subject = msg.Subject();
		string message = msg.Send();
		Console.WriteLine("Your Message has been sent successffully!!!");
		Console.WriteLine("\n");
		Console.WriteLine("To: {0}",reciever);
		Console.WriteLine("\n");
		Console.WriteLine("Subject: {0}",subject);
		Console.WriteLine("\n");
		msg.Receive(message);
		Console.WriteLine("From: {0}",sender);
		Console.WriteLine("\n");
	}
}