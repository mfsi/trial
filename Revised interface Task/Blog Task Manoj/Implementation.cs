using System;
using Interface;
using MyBlog;
public class Implementation
{
	public static void Main()
	{
		IMessage obj = new Blog();
		string text = obj.Send();
		obj.Receive(text);
		Console.Read();
	}
}