using Interface;
using System;
namespace MyBlog
{
	public class Blog:IMessage
	{
		string[] text = new string[1000];
		string[] dt = new string[1000];
		int i=0;
		int id=0;
		string str = String.Empty;
		public void Receive(string message)
		{
			Console.WriteLine("You have posted: "+message+"\n");
			crudOperation();
		}

		public string Send()
		{
			return Read();
		}

		public void ContentInBlog()
		{
			Console.WriteLine("\nAfter posting information on blog: ");
			for(int j=0;j<i;j++){
				if(text[j]!=null)
				{
					Console.WriteLine("......................................................");
					Console.WriteLine("Post ID: "+(j+1)+" : "+text[j]);
					Console.WriteLine("\nPosted On : "+dt[j]);
				}
			}
		}
		
		public string Read()
		{
			Console.WriteLine("Enter information that you want to post in your Blog: ");
			string read=Console.ReadLine();
			text[i] = read;
			dt[i] = DateTime.Now.ToString();
			i++;
			return read;
		}
		
		public void delete()
		{
			bool flag=false;
			Console.WriteLine("Enter blog id to be deleted: ");
			string str = Console.ReadLine();
			if(str!="")
			{
				flag = Int32.TryParse(str,out id);
				if(flag)
					delete(id);
				else
					Console.WriteLine("No such id present");
			}
		}
		
		public void edit()
		{
			bool flag=false;
			Console.WriteLine("Enter blog id to be edited: ");
			str = Console.ReadLine();
			if(str!="")
			{
				flag = Int32.TryParse(str,out id);
				if(flag)
					editPost(id);
				else
					Console.WriteLine("No such id present");
			}
		}
		
		public void crudOperation()
		{
			Console.WriteLine("1: Post Blog");
			Console.WriteLine("2: Display Blog");
			Console.WriteLine("3: Edit Blog");
			Console.WriteLine("4: Delete Blog");
			Console.WriteLine("5: Exit Blog");
			Console.WriteLine("Enter your choise:");
			string choise = Console.ReadLine();
			switch(choise)
			{
				case "1":
					string message = Read();
					Receive(message);
					break;
				case "2":
					ContentInBlog();
					Console.WriteLine("......................................................");
					crudOperation();
					break;
				case "3":
					edit();
					crudOperation();
					break;
				case "4":
					delete();
					crudOperation();
					break;
				default:
					Console.WriteLine("Sorry Invalid choise");
					Environment.Exit(0);
					break;
			}
		}
		
		public void delete(int postId){
			if(postId>0||postId<=i)
				text[postId-1]=null;
		}
		
		public void editPost(int postId)
		{
			text[postId-1] = Console.ReadLine();
			dt[postId-1] = DateTime.Now.ToString();
		}
	}
}