using System;
interface IMessage
{
 void SetConnection(string conn);
 string GetConnection();
 void Read(string a);
 string Write( );
 void Send(string b);
 string Receive();
}

public class Blog:IMessage{
	string connect;
	string message;
	public void SetConnection(string conn)
	{
		connect = conn;
	}
	public string GetConnection()
	{
		return connect;
	}
	public void Read(string a)
	{
		Console.WriteLine(a);
	}
	public string Write( )
	{
		Console.WriteLine("Write You Post: ");
		message = Console.ReadLine();
		return message;
	}
	public void Send(string b)
	{
		if(b=="connection")
		{
			Console.WriteLine(b+" is established.\n IT'S NOW SAFE TO POST THE BLOG");
		}
		else{
			Console.WriteLine("Sorry, CONNECTION CAN NOT BE ESTABLISHED.");
			Environment.Exit(0);
		}
	}
	public string Receive()
	{
		return ("BLOG IS POSTED: "+message);
	}
}

public class BlogImpl
{
	public static void Main()
	{
		string con;
		IMessage im = new Blog();
		Console.WriteLine("WANT TO POST BLOG (y/n): ");
		string choise = Console.ReadLine().Trim().ToLower();
		if(choise=="y")
			con="connection";
		else
			con="sorry";
		im.SetConnection(con);
		string conn = im.GetConnection();
		im.Send(conn);
		
		string text = im.Write();
		im.Read(text);
		Console.WriteLine(im.Receive());
	}
}