
interface IMessage
{
 boolean SetConnection(string conn);
 string GetConnection();
 void Read(string a);
 string Write();
 void Send(string b);
 string Receive();
}


class VoiceMessage:IMessage
{
  
  private string recordedmessage;
  boolean SetConnection()
  {
   Console.WriteLine("Setting Connection"); 
  }
  
  string Record()
  {
   Console.WriteLine("Recording voice");
   return recordedmessage;
  }
  


}

class program
{
    string str;
   VoiceMessage m=new VoiceMessage();
    str=m.Record();
	m.SetConnection();
	m.Send();
	m.Receive();
}