//Interface Assinment
using System;
interface IMessage
{
	void SetConnection(string con);
	string GetConnection();
	void Read(string a);
	string Write( );
	void Send(string b);
	string Receive(string c);
}

public class Email: IMessage
{
	string message= String.Empty;
	
	//Getting Connection
	public string GetConnection()
	{
		Console.WriteLine("Do you want to set a connection? Press YES or NO");
		string response = Console.ReadLine();		
		if (response == "YES")
		{
			Console.WriteLine("Your Request has been recieved.");
			return "true";
		}
		else
		{
			return "false";
		}
	}

	//Setting Connection
	public void SetConnection(string reply)
	{
		if(reply=="true")
		{
			Console.WriteLine("Connection Established");
		}
		else
		{
			Console.WriteLine("Connection could not be Established");
		}
		
	}
	
	//Read Message
	public void Read(string m3)
	{
		Console.WriteLine("Your message is:{0}", m3);
		//Console.Write(m3);
	}
	
	// Write Message
	public string Write()
	{
		Console.Write("Write your message here:");
		string m1= Console.ReadLine();
		return m1;
	}
	
	// Receive Message
	public void Send(string m)
	{
		string m2= m;
		Console.WriteLine("Your message:{0}  has been send successfully.", m2);
		
	}
	
	// Receive Message
	public string Receive(string m4)
	{
		Console.WriteLine("You have received a message");
		Console.WriteLine("Write YES to view your message");
		string reply = Console.ReadLine();
		return reply;
		
	}


	public static void Main(string[] args)
	{
		Email msg = new Email();
		string reply = msg.GetConnection();
		msg.SetConnection(reply);
		string message= msg.Write();
		msg.Send(message);
		string ans = msg.Receive(message);
		if (ans == "YES")
		{
			msg.Read(message);
		}
	}
	
}