using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;

namespace DelegatesEvents
{
  

    public class Customer : IComparable
    {
        protected Guid? _UniqueId;
        //local member variable which stores the object's UniqueId


        public static int val = 0;

        protected string _FirstName = "";
        protected string _LastName = "";
        protected int _CustomerId = 0;
        protected int _age = 0;
        protected string _address = "";

     

        //Default constructor
        public Customer()
        {

            //create a new unique id for this business object
            _UniqueId = Guid.NewGuid();

        }

        //Paramaterized constructor for immediate instantiation
        public Customer(string first, string last, int id, int age, string add)
        {
            _FirstName = first;
            _LastName = last;
            _CustomerId = id;
            _age = age;
            _address = add;

        }

        //UniqueId property for every business object
        public Guid? UniqueId
        {
            get
            {
                return _UniqueId;
            }
            set
            {
                _UniqueId = value;
            }
        }

        //Customer' First Name 
        public string FirstName
        {
            get
            {
                return _FirstName;
            }
            set
            {
                _FirstName = value;
            }
        }

        //Customer's Last Name
        public string LastName
        {
            get
            {
                return _LastName;
            }
            set
            {
                _LastName = value;
            }
        }

        //Customer's CustomerID
        public int CustomerID
        {
            get
            {
                return _CustomerId;
            }
            set
            {
                _CustomerId = value;
            }
        }

        //Customer's age
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                _age = value;
            }
        }

        //Customer's address
        public string Address
        {
            get
            {
                return _address;
            }
            set
            {
                _address = value;
            }
        }


        public int CompareTo(object obj)
        {
            Customer other = obj as Customer;
            switch (Customer.val)
            {

                case 1:
                    //Customer other = obj as Customer;
                    if (this.CustomerID > other.CustomerID) return 1;
                    else if (this.CustomerID < other.CustomerID) return -1;
                    else return 0;

                case 2:
                    /*if ( this.FirstName > other.FirstName ) return 1;
                    else if ( this.FirstName < other.FirstName ) return -1;
                    else return 0;*/

                    int response1 = string.Compare(FirstName, other.FirstName);
                    return response1;


                case 3:
                    /*if ( this.LastName > other.LastName ) return 1;
                    else if ( this.LastName < other.LastName ) return -1;
                    else return 0;*/

                    int response2 = string.Compare(LastName, other.LastName);
                    return response2;

                case 4:
                    if (this.Age > other.Age) return 1;
                    else if (this.Age < other.Age) return -1;
                    else return 0;

                default:
                    if (this.CustomerID > other.CustomerID) return 1;
                    else if (this.CustomerID < other.CustomerID) return -1;
                    else return 0;
            }

        }


    }

    public class CustomerObjectCollection<T> : ICollection<T> where T : Customer
    {
        protected List<Customer> _innerList;

        public delegate void ItemChanged(object customer);
        // an instance of the delegate with event keyword added
        public event ItemChanged _ItemChanged;

        public CustomerObjectCollection()
        {
            _innerList = new List<Customer>();
        }

        // Default accessor for the collection 
        public T this[int index]
        {
            get
            {
                return (T)_innerList[index];
            }
            set
            {
                _innerList[index] = value;
            }
        }

        // Number of elements in the collection
        public virtual int Count
        {
            get
            {
                return _innerList.Count;
            }
        }

        // Flag sets whether or not this collection is read-only
        public virtual bool IsReadOnly
        {
            get
            {
                return IsReadOnly;
            }
        }

       public void Run()
        {
            if (_ItemChanged != null)
            {
                _ItemChanged(this);
            }
        }


        // Add a Customer object to the collection
        public virtual void Add(T CustomerObject)
        {
            _innerList.Add(CustomerObject);
            if (_ItemChanged != null)
            {
                _ItemChanged(this);
            }

        }

        // Remove first instance of a customer object from the collection 
        public virtual bool Remove(T CustomerObject)
        {
            bool result = false;

            //loop through the inner array's indices
            for (int i = 0; i < _innerList.Count; i++)
            {
                //store current index being checked
                T obj = (T)_innerList[i];

                //compare the CustomerObject UniqueId property
                if (obj.CustomerID == CustomerObject.CustomerID)
                {
                    //remove item from inner ArrayList at index i
                    _innerList.RemoveAt(i);
                    result = true;
                    break;
                }
            }

            return result;
        }

        // Returns true/false based on whether or not it finds the requested object in the collection.
        public bool Contains(T CustomerObject)
        {
            //loop through the inner ArrayList
            foreach (T obj in _innerList)
            {
                //compare the CustomerObjectBase UniqueId property
                if (obj.UniqueId == CustomerObject.UniqueId)
                {

                    //if it matches return true
                    return true;
                }
            }
            //no match
            return false;
        }

        // Copy objects from this collection into another array
        public virtual void CopyTo(T[] CustomerObjectList, int index)
        {
            throw new Exception(
              "This Method is not valid for this implementation.");
        }

        // Clear the collection of all it's elements
        public virtual void Clear()
        {
            _innerList.Clear();
        }

        // Returns custom generic enumerator for this BusinessObjectCollection
        public virtual IEnumerator<T> GetEnumerator()
        {
            //return a custom enumerator object instantiated to use this BusinessObjectCollection 
            return new CustomerObjectEnumerator<T>(this);
        }

        // Explicit non-generic interface implementation for IEnumerable extended and required by ICollection (implemented by ICollection<T>)
        IEnumerator IEnumerable.GetEnumerator()
        {
            return new CustomerObjectEnumerator<T>(this);
        }


        //Sort
        public void Sort(int s)
        {
            Customer.val = s;
            _innerList.Sort();
        }


    }


    public class CustomerObjectEnumerator<T> : IEnumerator<T> where T : Customer
    {
        protected CustomerObjectCollection<T> _collection; //enumerated collection
        protected int index; //current index
        protected T _current; //current enumerated object in the collection

        // Default constructor
        public CustomerObjectEnumerator()
        {
            //nothing
        }

        public CustomerObjectEnumerator(CustomerObjectCollection<T> collection)
        {
            _collection = collection;
            index = -1;
            _current = default(T);
        }


        // Current Enumerated object in the inner collection
        public virtual T Current
        {
            get
            {
                return _current;
            }
        }

        // Explicit non-generic interface implementation for IEnumerator (extended and required by IEnumerator<T>)
        object IEnumerator.Current
        {
            get
            {
                return _current;
            }
        }

        // Dispose method
        public virtual void Dispose()
        {
            _collection = null;
            _current = default(T);
            index = -1;
        }

        // Move to next element in the inner collection
        public virtual bool MoveNext()
        {
            //make sure we are within the bounds of the collection
            if (++index >= _collection.Count)
            {
                //if not return false
                return false;
            }
            else
            {
                //if we are, then set the current element
                //to the next object in the collection
                _current = _collection[index];
            }
            //return true
            return true;
        }

        // Reset the enumerator
        public virtual void Reset()
        {
            _current = default(T); //reset current object
            index = -1;
        }

    }

    public class CustomerComparer : IComparer<Customer>
    {
        public int Compare(Customer x, Customer y)
        {
            // Invoking a CompareTo method to compare each item x of the collection with the current item y specified
            return x.CompareTo(y);
        }
    }



    // A subscriber: AddItem subscribes to the Customers's events. The job of AddItemis to notify when a new item is added.
    public class AddItem
    {
        public void Subscribe(CustomerObjectCollection<Customer> thecustomer)
        {
            thecustomer._ItemChanged += itemAdded;
        }

        public void itemAdded(object theCustomer)
        {
            MessageBox.Show("An Item has been added.");
            //Console.WriteLine("An Item has been added.");
        }
    }

    // A second subscriber whose job is to notify when an item is removed.
    public class RemoveItem
    {
        public void Subscribe(CustomerObjectCollection<Customer> thecustomer)
        {
            thecustomer._ItemChanged += itemRemoved;
        }

        public void itemRemoved(object theCustomer)
        {
            MessageBox.Show("An Item has been removed");
            //Console.WriteLine("An Item has been removed.");
        }
    }




    internal class Program
    {
        private static void Main(string[] args)
        {
            var customerObjList = new CustomerObjectCollection<Customer>();

        Start:
            Console.WriteLine("Enter your Choice");
            Console.WriteLine("1. To Add a customer to the custom generic collection.");
            Console.WriteLine("2. To Display all customers in the custom generic collection.");
            Console.WriteLine("3. To Remove a customer from the custom generic collection.");
            Console.WriteLine("4. To Search a customer in the custom generic collection.");
            Console.WriteLine("5. To Sort the customer collection.");

            int choice = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("\n");


            switch (choice)
            {
                case 1:
                    Console.WriteLine("Enter the First Name of the Customer");
                    string fName = Console.ReadLine();
                    Console.WriteLine("\n");

                    Console.WriteLine("Enter the Last Name of the Customer");
                    string lName = Console.ReadLine();
                    Console.WriteLine("\n");

                    Console.WriteLine("Enter the Customer Id of the Customer");
                    int C_Id = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("\n");

                    Console.WriteLine("Enter the Age of the Customer");
                    int age = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("\n");

                    Console.WriteLine("Enter the Address of the Customer");
                    string address = Console.ReadLine();
                    Console.WriteLine("\n");
                    var obj = new Customer(fName, lName, C_Id, age, address);
                    customerObjList.Add(obj);

                    //Delegates and Events
                    AddItem dc = new AddItem();
                    dc.Subscribe(customerObjList);
                    customerObjList.Run( );



                    Console.WriteLine("After adding to list..");
                    Console.WriteLine("\n");
                    Console.WriteLine("Total count {0}", customerObjList.Count);
                    Console.WriteLine("----------------------------------------------------------------");
                    Console.WriteLine("CustomerID \t First Name \t Last Name \t Age \t Address");
                    Console.WriteLine("----------------------------------------------------------------");
                    foreach (var customer in customerObjList)
                    {
                        Console.WriteLine(customer.CustomerID + "\t\t" + customer.FirstName + "\t\t" + customer.LastName + "\t\t" + customer.Age + "\t" + customer.Address);
                        Console.WriteLine("\n");
                    }
                    Console.WriteLine("\n");

                    goto Start;

                case 2:
                    Console.WriteLine("CustomerID \t First Name \t Last Name \t Age \t Address");
                    Console.WriteLine("----------------------------------------------------------------");
                    foreach (var customer in customerObjList)
                    {
                        Console.WriteLine(customer.CustomerID + "\t\t" + customer.FirstName + "\t\t" + customer.LastName + "\t\t" + customer.Age + "\t" + customer.Address);
                        Console.WriteLine("\n");
                    }
                    Console.WriteLine("\n");
                    goto Start;

                case 3:
                    Console.WriteLine("Enter the Customer Id of the Customer you want to remove.");
                    int id = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("\n");

                    foreach (var customer in customerObjList)
                    {
                        if (id == customer.CustomerID)
                        {
                            var element = customer;
                            customerObjList.Remove(element);

                            //Delegates and Events
                            RemoveItem re = new RemoveItem();
                            re.Subscribe(customerObjList);
                            customerObjList.Run();
                        }
                    }



                    Console.WriteLine("after removing");
                    Console.WriteLine("\n");
                    Console.WriteLine("Total count {0}", customerObjList.Count);
                    Console.WriteLine("----------------");
                    Console.WriteLine("CustomerID \t First Name \t Last Name \t Age \t Address");
                    Console.WriteLine("----------------------------------------------------------------");
                    foreach (var customer in customerObjList)
                    {
                        Console.WriteLine(customer.CustomerID + "\t\t" + customer.FirstName + "\t\t" + customer.LastName + "\t\t" + customer.Age + "\t" + customer.Address);
                        Console.WriteLine("\n");
                    }
                    Console.WriteLine("\n");
                    goto Start;

                case 4:
                    Console.WriteLine("Enter the Customer Id of the customer you want to search");
                    int c_id = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("CustomerID \t First Name \t Last Name \t Age \t Address");
                    Console.WriteLine("----------------------------------------------------------------");
                    foreach (var customer in customerObjList)
                    {
                        Console.WriteLine(customer.CustomerID + "\t\t" + customer.FirstName + "\t\t" + customer.LastName + "\t\t" + customer.Age + "\t" + customer.Address);
                        Console.WriteLine("\n");
                    }
                    foreach (var customer in customerObjList)
                    {
                        if (c_id == customer.CustomerID)
                        {
                            var element = customer;
                            if (customerObjList.Contains(customer))
                            {
                                Console.WriteLine("Search matched");
                                Console.WriteLine(customerObjList[0].CustomerID + "\t\t" + customerObjList[0].FirstName + "\t\t" + customerObjList[0].LastName + "\t\t" + customerObjList[0].Age + "\t" + customerObjList[0].Address);
                                Console.WriteLine("\n");
                            }
                            else
                            {
                                Console.WriteLine("Customer not found");
                            }
                        }

                    }

                    goto Start;
                case 5:

                    Console.WriteLine("Enter 1 :: Sort By Cid || 2 :: Sort By FirstName || 3 :: Sort By LastName ||4 :: Sort By Age");
                    int selection = Convert.ToInt32(Console.ReadLine());
                    customerObjList.Sort(selection);
                    Console.WriteLine("CustomerID \t First Name \t Last Name \t Age \t Address");
                    Console.WriteLine("----------------------------------------------------------------");
                    foreach (var customer in customerObjList)
                    {
                        Console.WriteLine(customer.CustomerID + "\t\t" + customer.FirstName + "\t\t" + customer.LastName + "\t\t" + customer.Age + "\t" + customer.Address);
                        Console.WriteLine("\n");
                    }
                    Console.WriteLine("\n");

                    goto Start;
            }
        }
    }	

}
